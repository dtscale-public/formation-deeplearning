# Formation Deep Learning

## Comment préparer son environnement pour la formation Deep Learning ?

Afin de disposer d'un environnemnet python et des packages nécessaires pour la formation vous pouvez:
- utiliser une image docker fournie (nécessite les droits d'administration)
- vous créer un environnement python 3.6 et installer les packages nécessaires via miniconda et pip.

Dans le cas ou vous préférez la deuxième option, veuillez suivre les instructions dans la rubirque Miniconda de ce  [document](###miniconda)

### __**Docker**__

### Overview

*Temps estimé : 15-30 min*

1.  Installer Docker sur sa machine
1.  Récuper l'image de la formation sous Docker Hub
1.  Télécharger l'environnement de test
1.  Tester l'image

### Détails

#### Installer Docker (optionnel)

Pour verrifier si Docker est déjà installé sur votre machine, ouvrer un terminal et entrer la commande :

`docker -v`

##### Sous Windows

Installer docker sur windows:
pous pouvez suivre le tutoriel ici :
[https://docs.docker.com/docker-for-windows/install/](https://docs.docker.com/docker-for-windows/install/)

!! Attention pour installer Docker, vous devez disposer de droits administrateurs (le temps de l'installation), et de Windows 10 Professionnel.
!!! Pensez à activer l'option "partage de fichiers" en faisant clique droit du docker > Settings > Shared Drives > C puis apply
##### Sous Linux

Installer docker sur Mac:
pous pouvez suivre le tutoriel ici :
[https://runnable.com/docker/install-docker-on-linux](https://runnable.com/docker/install-docker-on-linux)

##### Sous Mac

Installer docker sur Mac:
pous pouvez suivre le tutoriel ici :
[https://docs.docker.com/docker-for-mac/install/](https://docs.docker.com/docker-for-mac/install/)

#### Télécharger l'image de la formation

Une fois docker installé, télécharger l'image depuis docker hub. 
Pour ce faire exécutez la commande suivante sur un terminal (en mode administrateur si besoin):

`docker pull pieleroy/formation_deep_learning:telespazio` 

#### Télécharger l'environnement de test


Télécharger le dépot à l'addresse suivante : [ici](https://gitlab.com/dtscale-public/formation-deeplearning/-/archive/master/formation-deeplearning-master.zip)

#### Tester l'image

Lancez avec la commande suivante :

`docker run -it -p 8888:8888 -p 6006:6006 -v *PATH_TO_FORMATION-DEEPLEARNING*:/home/ubuntu  -d pieleroy/formation_deep_learning:telespazio`

!! Remplacer *PATH_TO_FORMATION-DEEPLEARNING* par le chemin approprié.(sur windows pensez à mettre le chemin absolu avec des "/" et non des " \ " )

Une fois le docker lancé, vérifier le bon lancement du docker en vous connectant via un navigateur sur cet url : 

[http://localhost:8888/tree](http://localhost:8888/tree)

Depuis cet url; cliquez sur "new" à droite de l'écran **puis terminal**. Un terminal de commande apparait, depuis ce dernier exécutez le script test_import.py à l'aide de la commande suivante :

`python3 test_import.py`

Si le message "environnement ok" apparaît alors votre environnement est opérationnel. 
Pour toute question vous pouvez m'envoyer un mail à pierre.leroy@scalian.com

### __**Miniconda**__

### Overview

*Temps estimé : 20 min*

1.  Installer miniconda
1.  Installer l'environnement python
1.  Installer les packages
1.  Tester l'environnement

### Détails


#### Installer python 3.6.x (optionnel)

Pour installer rapidement python 3.6 vous pouvez utiliser miniconda. 


##### Sous Windows

Installer miniconda sur Windows:
[https://repo.continuum.io/miniconda/Miniconda3-4.3.31-Windows-x86_64.exe](https://repo.continuum.io/miniconda/Miniconda3-4.3.31-Windows-x86_64.exe)

Il faut suivre alors les instructions d'installions. Une fois l'installation terminée vous devez avoir un python 3.6 d'installé.
Pour le vérifier vous pouvez ouvrir un terminal et exécuter "python --version".
Si cela ne fonctionne pas vérifier l'installation et/ou que le chemin de python est bien ajouté au PATH (accesible sur windows via les variables d'environnement).

Vous pouvez le faire en ligne de commande via l'éxecution suivante : 
`SETX PATH "MINICONDA_PATH\AppData\Local\Continuum\miniconda3;MINICONDA_PATH\AppData\Local\Continuum\miniconda3\Scripts;"`

PS: penser à remplacer MINICONDA_PATH par votre chemin d'installation de miniconda

##### Sous Linux

Installer miniconda sur Linux:
pous pouvez suivre le tutoriel ici :
[https://repo.continuum.io/miniconda/Miniconda3-4.3.31-Linux-x86.sh](https://repo.continuum.io/miniconda/Miniconda3-4.3.31-Linux-x86.sh)

##### Sous Mac

Installer miniconda sur Mac:
pous pouvez suivre le tutoriel ici :
[https://repo.continuum.io/miniconda/Miniconda3-4.3.31-MacOSX-x86_64.sh](https://repo.continuum.io/miniconda/Miniconda3-4.3.31-MacOSX-x86_64.sh)



#### Télécharger le dépot 

Télécharger le dépot à l'adresse suivante : [ici](https://gitlab.com/dtscale-public/formation-deeplearning/-/archive/master/formation-deeplearning-master.zip)
afin de récupérer le fichier "environment-crossplatform.yml", "requirement.txt" ainsi que le fichier de test.

#### Créer un environnement virtuel avec conda :

Depuis un terminal exécutez la commande suivante : 

`conda env create -f environment.yml`

Cette commande va créer l'environnement python

#### Activer l'environnement
Afin d'activer l'environnement conda veuillez exécuter le commande suivante 

##### Sous windows
`activate formation-deeplearning`

##### Sous Linux
`source activate formation-deeplearning`

##### Sous Mac
`conda activate formation-deeplearning`

#### Installer les packages python

`pip install -r requirement.txt`


#### Tester l'environnement
Exécutez alors la commande suivante pour valider l'installation :
`python3 test_import.py`


Si le message "environnement ok" apparaît alors votre environnement est opérationnel. 
Pour toute question vous pouvez m'envoyer un mail à pierre.leroy@scalian.com

