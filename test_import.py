#!/usr/bin/env python 
#-*- coding: utf-8 -*-
try:
    
    import keras
    from keras.datasets import mnist
    from keras.models import Sequential
    from keras.layers import Dense, Dropout, Flatten
    from keras.layers import Conv2D, MaxPooling2D
    from keras import backend as K
    from sklearn.model_selection import train_test_split
    import numpy as np
    import cv2

    import sys
    import tensorflow as tf
    import os
    from sklearn.model_selection import train_test_split    
    print("environnement ok")
except Exception as e:
    print(e)
    print("erreur lors de l'import des packages")